<!doctype html>
<!--[if lt IE 7 ]> <html lang="en" class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
<meta name="viewport" content="width = 1050, user-scalable = no" />
<script type="text/javascript" src="../../extras/jquery.min.1.7.js"></script>
<script type="text/javascript" src="../../extras/modernizr.2.5.3.min.js"></script>
<script type="text/javascript" src="../../lib/hash.js"></script>
</head>
<body>

<div id="paper"></div>

<script src="//cdn.jsdelivr.net/turn.js/3/turn.js"></script>
<script type="text/javascript">
   $(window).ready(function() {
        $('#paper').turn({
    			// Width
    			width:922,
    			// Height
    			height:600,
    			// Elevation
    			elevation: 50,
    			// Enable gradients
    			gradients: true,
    			// Auto center this flipbook
    			autoCenter: true,
    			pages:10
        });
    });

      $('#paper').bind('turning', function(e, page) {
          var range = $(this).turn('range', page);
          Hash.go('page/'+page).update();
          for (page = range[0]; page<=range[1]; page++)
            addPage(page, $(this));
        });

        function addPage(page, book) {
           // Check if the page is not in the book
          if (!book.turn('hasPage', page)) {
            // Create an element for this page
            var element = $('<div />').html('Loading…');
            // Add the page
            book.turn('addPage', element, page);
            // Get the data for this page   
           $.ajax({url:"test.php?filename=abcd&page="+page})
             .done(function(data) {
              console.log(data);
             	// if(data.indexOf("loader") > -1 ){
              //      setTimeout(function() {
              //          refreshPage(element,page);
              //       }, 5000);
	             //  }
               element.html(data);
             });
           }
        }

</script>

</body>
</html>